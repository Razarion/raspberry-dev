import noble from 'noble';


// let peripheralAddress = 'A4:C1:38:8F:18:7A'.toLocaleLowerCase();
let peripheralAddress = 'A4:C1:38:67:2A:8E'.toLocaleLowerCase();

class Context {
    constructor() {
        this.displayServices = [];
    }

    addDisplayService(service) {
        let displayService = new DisplayService(service);
        this.displayServices.push(displayService);
        return displayService;
    }

    dump() {
        for (const displayService of this.displayServices) {
            displayService.dump();
        }
    }
}

class DisplayService {
    constructor(service) {
        this.service = service;
        this.displayCharacteristics = [];
    }

    getService() {
        return this.service;
    }

    addDisplayCharacteristic(characteristic) {
        let displayCharacteristic = new DisplayCharacteristic(characteristic);
        this.displayCharacteristics.push(displayCharacteristic);
        return displayCharacteristic;
    }

    dump() {
        console.log("--------------------------------")
        console.log("service: " + this.service.uuid)
        for (const displayCharacteristic of this.displayCharacteristics) {
            displayCharacteristic.dump();
        }
    }
}

class DisplayCharacteristic {
    constructor(characteristic) {
        this.characteristic = characteristic;
        this.data = [];
    }

    getCharacteristic() {
        return this.characteristic;
    }

    addDisplayData(data) {
        this.data.push(data);
    }

    dump() {
        console.log("- characteristic: " + this.characteristic.uuid)
        for (const data of this.data) {
            console.log('--- ' + data + ' "' + data.toString() + '"');
        }
    }

}

noble.on('discover', function (peripheral) {
    if (peripheral.address !== peripheralAddress) {
        return;
    }

    console.log('Peripheral with ID ' + peripheral.id + ' found');
    console.log('Uuid: ' + peripheral.uuid);
    console.log('State: ' + peripheral.state);
    console.log('Connectable: ' + peripheral.connectable);
    let context = new Context();

    peripheral.on('disconnect', function () {
        console.log('----------------- disconnect -----------------');
    });

    setTimeout(function () {
        context.dump();
        process.exit(0);
    }, 10000);

    peripheral.connect(function (error) {
        if (error != null) {
            console.log('error: ' + error);
            process.exit(0);
        }

        peripheral.discoverServices([], function (error, services) {
            if (error != null) {
                console.log('error: ' + error);
                process.exit(0);
            }
            for (const service of services) {
                let displayService = context.addDisplayService(service);
                handleService(displayService);
            }

            //         let service = findService(services, "000102030405060708090a0b0c0d1910");
            //         service.discoverCharacteristics([], function (error, characteristics) {
            //             if (error != null) {
            //                 console.log('error: ' + error);
            //                 process.exit(0);
            //             }
            //             let characteristic = findCharacteristic(characteristics, "000102030405060708090a0b0c0d1914");
            //             // characteristic.discoverDescriptors(function (error, descriptors) {
            //             //     if (error != null) {
            //             //         console.log('error: ' + error);
            //             //         process.exit(0);
            //             //     }
            //             //     descriptors[0].readValue(function (error, data) {
            //             //         if (error != null) {
            //             //             console.log('error: ' + error);
            //             //             process.exit(0);
            //             //         }
            //             //         console.log("Descriptor: " + data.toString());
            //             //     });
            //             // });
            //             let writeBuffer = Buffer.from([0x00, 0x3a, 0x3, 0x88, 0x2c, 0x00, 0x66, 0x73, 0x58, 0xb4, 0x7c, 0x74, 0x07, 0x44, 0x76, 0xf6, 0xcd]);
            //             characteristic.write(writeBuffer, true, function (error) {
            //                 if (error != null) {
            //                     console.log('error: ' + error);
            //                     process.exit(0);
            //                 }
            //                 console.log("------------ Written ------------");
            //             });
            //
            //             characteristic.read(function (error, data) {
            //                 if (error != null) {
            //                     console.log('error: ' + error);
            //                     process.exit(0);
            //                 }
            //                 console.log("------------ READ ------------");
            //                 console.log(data);
            //                 console.log(data.toString());
            //                 peripheral.disconnect(function (error) {
            //                     if (error != null) {
            //                         console.log('error: ' + error);
            //                     }
            //                     console.log('Disconnected');
            //                     // process.exit(0);
            //                 });
            //                 // process.exit(0);
            //             });
            //         });
            //
            //
        });
    });
});

function handleService(displayService) {
    displayService.getService().discoverCharacteristics([], function (error, characteristics) {
        if (error != null) {
            console.log('error: ' + error);
            process.exit(0);
        }

        for (const characteristic of characteristics) {
            handleCharacteristic(displayService.addDisplayCharacteristic(characteristic));
        }
    });
}

function handleCharacteristic(displayCharacteristic) {
    displayCharacteristic.getCharacteristic().read(function (error, data) {
        if (error != null) {
            console.log('error: ' + error);
            process.exit(0);
        }
        displayCharacteristic.addDisplayData(data);
    });
}

function findService(services, uuid) {
    // for (const service of services) {
    //     console.log("service: " + service.uuid)
    // }
    for (const service of services) {
        if (service.uuid === uuid) {
            return service;
        }
    }
    throw "No service for UUID found: " + uuid;
}

function findCharacteristic(characteristics, uuid) {
    // for (const characteristic of characteristics) {
    //     console.log("characteristic: " + characteristic.uuid)
    // }
    for (const characteristic of characteristics) {
        if (characteristic.uuid === uuid) {
            return characteristic;
        }
    }
    throw "No Characteristic for UUID found: " + uuid;
}

console.log('--- Start scanning ---');
noble.startScanning();

